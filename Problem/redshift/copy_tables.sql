-- First you need to CREATE all the tables
-- Useful Guides:
-- https://docs.aws.amazon.com/redshift/latest/dg/r_COPY.html
-- https://docs.aws.amazon.com/redshift/latest/dg/r_COPY_command_examples.html
-- https://docs.aws.amazon.com/redshift/latest/dg/automatic-recognition.html
-- Try to use IAM and not Keys
-- 
-- Useful command for check errors
-- select * from pg_catalog.stl_load_errors
