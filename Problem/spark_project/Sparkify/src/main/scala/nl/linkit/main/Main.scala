package nl.linkit.main

import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}

import nl.linkit.utils._

object Main {

  def main(args: Array[String]): Unit = {
    val params = new ProcessParams(args)
    val config = getConfig(params)
    val sparkProject = new Sparkify(params, config)
    sparkProject.execute()
  }

  def getConfig(params: ProcessParams): Config = {
    ConfigFactory
      .parseResources(params.env() + ".conf")
      .resolve()
      .getConfig("application")
  }

}
