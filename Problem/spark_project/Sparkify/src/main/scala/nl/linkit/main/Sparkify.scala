package nl.linkit.main

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import nl.linkit.utils._

import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}

class Sparkify(params: ProcessParams, config: Config) {

  implicit val spark: SparkSession =
    SparkUtils.getSparkSession(s"Sparkify")

  def execute(): Unit = {
    println("Environment: " + params.env())
    try {
      if (params.env.isEmpty) params.printHelp()
      else
        executeProject()
    } catch {
      case e: Exception =>
        println(e.getMessage)
        throw e
    } finally {
      spark.stop()
    }
  }

  def executeProject(): Unit = {
    val song_data_path = config.getString("input.song_data")
    val log_data_path = config.getString("input.log_data")

    val songs_path = config.getString("output.songs")
    val artists_path = config.getString("output.artists")
    val time_path = config.getString("output.time")
    val users_path = config.getString("output.users")
    val songsplays_path = config.getString("output.songplays")

    val songDataDF = spark.read.json(song_data_path).filter(col("year") > 0).cache()

    val song_table_DF = songDataDF
      .select("song_id",
        "title",
        "artist_id",
        "year",
        "duration")

    println("Writing: Songs table")
    song_table_DF.write.mode("overwrite")
      .partitionBy("year", "artist_id")
      .parquet(songs_path)

    val artists_table = songDataDF
      .select(
        col("artist_id"),
        col("artist_name").alias("name"),
        col("artist_location").alias("location"),
        col("artist_latitude").alias("latitude"),
        col("artist_longitude").alias("longitude")
      )

    println("Writing: Artists table")
    artists_table.write.mode("overwrite").parquet(artists_path)

    val logDataDF = spark.read.json(log_data_path).cache()

    val log_filter_DF = logDataDF
      .filter(col("page") === "NextSong")
      .filter(col("userId").isNotNull)

    val users_table_DF = log_filter_DF
      .select(
        col("userId").alias("user_id"),
        col("firstName").alias("first_name"),
        col("lastName").alias("last_name"),
        col("gender"),
        col("level")
      )

    println("Writing: Users table")
    users_table_DF.write.mode("overwrite").parquet(users_path)

    val time_table_DF = log_filter_DF
      .withColumn("datetime", (col("ts") / 1000).cast(TimestampType))
      .withColumn("hour", hour(col("datetime")))
      .withColumn("day", dayofmonth(col("datetime")))
      .withColumn("week", weekofyear(col("datetime")))
      .withColumn("month", month(col("datetime")))
      .withColumn("year", year(col("datetime")))
      .withColumn("weekday", date_format(col("datetime"), "F"))

    println("Writing: Time table")
    time_table_DF
      .select("datetime", "hour", "day", "week", "month", "year", "weekday")
      .write.mode("overwrite")
      .partitionBy("year", "month")
      .parquet(time_path)

    val songplays_table_DF = songDataDF
      .join(log_filter_DF, songDataDF.col("artist_name") === log_filter_DF.col("artist"))
      .withColumn("songplay_id", monotonically_increasing_id())
      .withColumn("start_time", (col("ts") / 1000).cast(TimestampType))
      .select(col("songplay_id"),
        col("start_time"),
        col("userId").alias("user_id"),
        col("level"),
        col("song_id"),
        col("artist_id"),
        col("sessionId").alias("session_id"),
        col("artist_location").alias("location"),
        col("userAgent"),
        month(col("start_time")).alias("month"),
        year(col("start_time")).alias("year")
      )

    println("Writing: Songsplays table")
    songplays_table_DF.write.mode("overwrite")
      .partitionBy("year", "month")
      .parquet(songsplays_path)

    songDataDF.unpersist()
    log_filter_DF.unpersist()

  }

}

